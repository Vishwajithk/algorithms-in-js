var array = [10,8,3,2,89,4,9,5];
function bubble_sort(array)
{
    var n1 = new Date().getTime();
    console.log(n1*(1e+6));
    var i,j,limit;
    limit=array.length;
    while(limit--)
    {
        for(i=0,j=1;i<limit;i++,j++)
        {
            if(array[i]>array[j])
            {
                temp=array[i];
                array[i]=array[j];
                array[j]=temp;
            }
        }
    }
    var n2 = new Date().getTime();
    console.log(n2*(1e+6));
    var result = n2-n1;
    console.log("Bubble sort took "+ result + " nano-seconds");
    console.log("-----------------------------------------------");
    console.log("The sorted array is");
    return array;
}
console.log(bubble_sort(array));