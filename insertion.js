var insertion_sort = function(arr) 
{
    var n1 = new Date().getTime();
    console.log(n1*(1e+6));
    var length = arr.length;
    for (var i = 1; i < length; i++) 
    {
        var key = arr[i];
        var j = i - 1;
        while (j >= 0 && arr[j] > key) 
        {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
    var n2 = new Date().getTime();
    console.log(n2*(1e+6));
    var result = n2-n1;
    console.log("Insertion sort took " + result + " nano-seconds");
    console.log("----------------------------------------------------");
    console.log("The sorted array is");
    return arr;
};

var array = [10,8,3,2,89,4,9,5];
console.log(insertion_sort(array));