
function quick_sort(arr)
{
    if(arr.length<=1)
    return arr;
    
    var n1 = new Date().getTime();
    console.log(n1*(1e+6));
    var pivot=arr[arr.length-1];
    console.log("pivot value of each iteration is "+ pivot);
    var left = [];
    var right = [];
    for(var i=0;i<arr.length-1;i++)
    {
        console.log(arr);
        if(arr[i]<pivot)
        {
            left.push(arr[i]);
        }
        else
        {
            right.push(arr[i]);
        }
    }
    var n2 = new Date().getTime();
    console.log(n2*(1e+6));
    var result = n2-n1;
    console.log("Quick sort took "+ result + " nano-seconds");
    console.log("---------------------------------");
    console.log("The sorted array is");
    return [...quick_sort(left),pivot,...quick_sort(right)]
}

var array = [10,8,3,2,89,4,9,5];
console.log(quick_sort(array));
