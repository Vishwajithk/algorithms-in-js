
var array = [10,8,3,2,89,4,9,5];
var n1,n2;
function seq_search(arr,value)
{
    n1 = new Date().getTime();
    console.log(n1*(1e+6));
    for(var i=0;i<arr.length;i++)
    {
        if(arr[i]==value)
        {
            return i;
        }
    }
    return -1;
}

var found = seq_search(array,9);
n2 = new Date().getTime();
console.log(n2*(1e+6));
var result = n2-n1;
console.log("Linear search took " + result + " nano-seconds");
console.log("-----------------------------------");
console.log("The number is found in the " + found + " position");
console.log("-----------------------------------");