
var selection_sort = function(array)
{
  var n1 = new Date().getTime();
  console.log(n1*(1e+6));
  for(var i = 0; i < array.length; i++)
  {
    var min = i;
    for(var j = i+1; j < array.length; j++)
    {
      if(array[j] < array[min])
      {
        min = j;
      }
    }
    var temp = array[i];
    array[i] = array[min];
    array[min] = temp;
  }
  var n2 = new Date().getTime();
  console.log(n2*(1e+6));
  var result = n2-n1;
  console.log("Selection sort took "+ result + " nano-seconds");
  console.log("----------------------------------------------------");
  console.log("The sorted array is");
  return array;
};
var array = [10,8,3,2,89,4,9,5];
console.log(selection_sort(array));